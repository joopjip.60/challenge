#
# Build stage
#
FROM maven:3.8.3-openjdk-17 AS build
COPY src /home/app/src
COPY pom.xml /home/app
RUN mvn -f /home/app/pom.xml clean package

#
# Package stage
#
FROM maven:3.8.3-openjdk-17
WORKDIR /sample
COPY src /sample/src
COPY pom.xml /sample
RUN mvn -f /sample/pom.xml clean package
RUN mvn surefire-report:report
ENTRYPOINT ["java","-jar","/sample/target/challenge-0.0.1-SNAPSHOT.jar"]