# Challenge

### How to start execute the application in the docker

1. Clone the project from
   ```https://gitlab.com/joopjip.60/challenge```
2. Build Docker image
   ```docker build -t demo/myapp .```
3. Create the bridge between container tobe ready for the test framework
   ```docker network create tulip-net```
4. Start the application with the connection bridge
   ```docker run --rm -p 8080:8080 --net tulip-net --name demoapp demo/myapp```
5. All the unit test results are in ```target/site/surefire-report.html```
