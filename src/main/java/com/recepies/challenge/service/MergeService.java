package com.recepies.challenge.service;

import com.recepies.challenge.model.category.Category;
import com.recepies.challenge.model.content.Content;
import com.recepies.challenge.model.content.ContentRoot;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class MergeService {
    public List<Content> mergedContentList = new ArrayList<>();

    public List<Content> getCategorySum(List<Category> categories, ContentRoot contentRoot) {
        List<Content> mergedContentList = new ArrayList<>();
        for (Content content : contentRoot.documents) {
            List<String> pathList = new ArrayList<>();
            for (Category category : categories) {
                if (content.getDocument().getElements().getCategory() != null
                        && content.getDocument().getElements().getCategory().getCategoryIds() != null
                        && content.getDocument().getElements().getCategory().categoryIds.contains(category.getId())) {
                    pathList.add(category.getPath());
                }
            }
            if (!pathList.isEmpty()) content.getDocument().getElements().getCategory().setPath(pathList);
            mergedContentList.add(content);
        }
        this.mergedContentList = mergedContentList;
        return mergedContentList;
    }
}
