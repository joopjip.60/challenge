package com.recepies.challenge.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.recepies.challenge.model.category.Category;
import com.recepies.challenge.model.categoryCount.CategoryCount;
import com.recepies.challenge.model.content.Content;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Service
public class CountDocumentService {

    @Autowired
    MergeService mergeService;
    @Autowired
    CategoryService categoryService;

    public List<CategoryCount> documentPerCategory() {
        List<Content> documents = mergeService.mergedContentList;
        List<Category> categories = categoryService.categories;
        List<CategoryCount> categoryCountList = new ArrayList<>();
        for (Category category : categories) {
            CategoryCount categoryCount = new CategoryCount();
            int count = 0;
            for (Content document : documents) {
                if (document.getDocument().getElements().getCategory() != null
                        && document.getDocument().getElements().getCategory().getCategoryIds() != null
                        && document.getDocument().getElements().getCategory().categoryIds.contains(category.getId())) {
                    count++;
                }
            }
            categoryCount.setCategory_id(category.getId());
            categoryCount.setPath(category.getPath());
            categoryCount.setDocuments(count);
            categoryCountList.add(categoryCount);
            writeToFile(categoryCountList);
        }
        return categoryCountList;
    }

    private void writeToFile(List<CategoryCount> categoryCountList) {
        try (FileWriter file = new FileWriter("count.json")) {
            ObjectMapper mapper = new ObjectMapper();
            String json = mapper.writeValueAsString(categoryCountList);
            file.write(json);
            file.flush();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
