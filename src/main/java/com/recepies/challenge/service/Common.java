package com.recepies.challenge.service;

import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Scanner;

public class Common {

    public String getDataFromEndPoint(String urlString) {
        HttpURLConnection conn = null;
        String output = "";
        try {
            URL url = new URL(urlString);
            conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            conn.setRequestProperty("Accept", "*/*");
            if (conn.getResponseCode() != 200) {
                throw new RuntimeException("Failed : HTTP error code : "
                        + conn.getResponseCode());
            }
            Scanner s = new Scanner(new InputStreamReader(conn.getInputStream())).useDelimiter("\\A");
            output = s.hasNext() ? s.next() : "";
            conn.disconnect();

        } catch (IOException e) {
            System.out.println(e);
        }
        return output;
    }
}
