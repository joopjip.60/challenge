package com.recepies.challenge.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.recepies.challenge.model.category.Category;
import com.recepies.challenge.model.category.CategoryRoot;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CategoryService {

    public List<Category> categories;
    private Common common = new Common();

    public CategoryService() {
    }

    public void getCategoryFromEndPoint() {
        ObjectMapper mapper = new ObjectMapper();
        CategoryRoot categoryRoot = null;
        try {
            categoryRoot = mapper.readValue(common.getDataFromEndPoint("https://content-us-1.content-cms.com/api/06b21b25-591a-4dd3-a189-197363ea3d1f/delivery/v1/search?q=classification:category&rows=100"), CategoryRoot.class);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        this.categories = categoryRoot.documents;
    }

}
