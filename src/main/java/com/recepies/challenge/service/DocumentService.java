package com.recepies.challenge.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.recepies.challenge.model.content.ContentRoot;
import org.springframework.stereotype.Service;

@Service
public class DocumentService {

    public ContentRoot contentRoot;
    private Common common = new Common();

    public void getDocumentFromEndpoint() {
        ObjectMapper mapper = new ObjectMapper().configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        ContentRoot contentRoot = null;
        try {
            contentRoot = mapper.readValue(common.getDataFromEndPoint("https://content-us-1.content-cms.com/api/06b21b25-591a-4dd3-a189-197363ea3d1f/delivery/v1/search?q=classification:content&fl=document:%5bjson%5d&fl=type&rows=100"), ContentRoot.class);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        this.contentRoot = contentRoot;
    }
}
