package com.recepies.challenge.model.content.elements;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
class Large {
    public Transform transform;
    public String renditionId;
    public int width;
    public String source;
    public int height;
    public String url;
}
