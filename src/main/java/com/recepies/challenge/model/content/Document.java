package com.recepies.challenge.model.content;

import com.recepies.challenge.model.content.elements.Elements;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

@AllArgsConstructor
@Data
public class Document {
    String rev;
    Thumbnail thumbnail;
    List<String> kind;
    String created;
    String creatorId;
    String description;
    String classification;
    String type;
    List<String> tags;
    List<Object> selectedLayouts;
    Elements elements;
    String name;
    String lastModifierId;
    String typeId;
    Object links;
    String id;
    String lastModified;
    String systemModified;
    String status;

    public Document() {
    }
}