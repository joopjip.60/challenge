package com.recepies.challenge.model.content.elements;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Category {
    public List<String> categoryIds;
    public List<String> categories;
    public String elementType;
    public List<String> path;
}
