package com.recepies.challenge.model.content.elements;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
class Image {
    public List<String> profiles;
    public Renditions renditions;
    public Asset asset;
    public String elementType;
    public String url;
}
