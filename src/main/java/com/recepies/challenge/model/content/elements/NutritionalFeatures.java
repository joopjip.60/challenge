package com.recepies.challenge.model.content.elements;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
class NutritionalFeatures {
    public List<String> categoryIds;
    public List<String> categories;
    public String elementType;
}
