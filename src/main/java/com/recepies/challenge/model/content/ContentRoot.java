package com.recepies.challenge.model.content;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.ToString;

import java.util.List;

@ToString
@AllArgsConstructor
@Data
public class ContentRoot {

    public int numFound;
    public List<Content> documents;

    public ContentRoot() {
    }
}