package com.recepies.challenge.model.content;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Content {
    String type;
    Document document;

    public Content() {
    }
}
