package com.recepies.challenge.model.content.elements;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
class Asset {
    public String fileName;
    public String altText;
    public int fileSize;
    public int width;
    public String mediaType;
    public String id;
    public String resourceUri;
    public int height;
}
