package com.recepies.challenge.model.content;

import lombok.AllArgsConstructor;
import lombok.Data;

@AllArgsConstructor
@Data
public class Thumbnail {
    String id;
    String url;

    public Thumbnail() {
    }
}
