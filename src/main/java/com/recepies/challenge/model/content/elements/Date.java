package com.recepies.challenge.model.content.elements;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
class Date {
    public String elementType;
    public String value;

}
