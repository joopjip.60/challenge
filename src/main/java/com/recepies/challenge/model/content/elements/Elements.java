package com.recepies.challenge.model.content.elements;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Elements {
    public Method method;
    public Author author;
    public Image image;
    public Description description;
    public Date date;
    public NutritionalFeatures nutritionalFeatures;
    public Link link;
    public Ingredients ingredients;
    public Category category;
    public Title title;
}



