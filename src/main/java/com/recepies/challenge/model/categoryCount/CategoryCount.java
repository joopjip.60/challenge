package com.recepies.challenge.model.categoryCount;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CategoryCount {
    String category_id;
    String path;
    int documents;
}
