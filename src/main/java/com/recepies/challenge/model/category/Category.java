package com.recepies.challenge.model.category;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Category {

    String id;
    String name;
    String classification;
    String lastModified;
    String created;
    String creatorId;
    String path;

    public Category() {
    }
}