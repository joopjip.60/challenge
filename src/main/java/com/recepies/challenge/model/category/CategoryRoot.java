package com.recepies.challenge.model.category;

import lombok.AllArgsConstructor;

import java.util.List;


@AllArgsConstructor
public class CategoryRoot {

    public int numFound;
    public List<Category> documents;

    public CategoryRoot() {
    }
}