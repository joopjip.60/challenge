package com.recepies.challenge.applicationStartUp;

import com.recepies.challenge.service.CategoryService;
import com.recepies.challenge.service.DocumentService;
import com.recepies.challenge.service.MergeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component
public class ApplicationStartUp {

    @Autowired
    private CategoryService categoryService;
    @Autowired
    private DocumentService documentService;
    @Autowired
    private MergeService mergeService;

    /*
      Before the application start
      Category Object is created
      Content object is created
     */

    @PostConstruct
    public void initialize() {
        documentService.getDocumentFromEndpoint();
        categoryService.getCategoryFromEndPoint();
        mergeService.getCategorySum(categoryService.categories, documentService.contentRoot);
    }


}