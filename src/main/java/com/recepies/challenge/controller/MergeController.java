package com.recepies.challenge.controller;

import com.recepies.challenge.model.content.Content;
import com.recepies.challenge.service.MergeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api")
public class MergeController {
    @Autowired
    MergeService mergeService;

    @GetMapping(path = "/merge")
    public List<Content> getMerge() {
        return mergeService.mergedContentList;
    }
}
