package com.recepies.challenge.controller;

import com.recepies.challenge.model.categoryCount.CategoryCount;
import com.recepies.challenge.service.CountDocumentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api")
public class CountDocumentController {
    @Autowired
    CountDocumentService countDocumentService;

    @GetMapping(path = "/count")
    public List<CategoryCount> getCount() {
        return countDocumentService.documentPerCategory();
    }
}
