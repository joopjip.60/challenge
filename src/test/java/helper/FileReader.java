package helper;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class FileReader {
    public String readFile(String path) throws FileNotFoundException {
        File myObj = new File(path);
        Scanner myReader = new Scanner(myObj);
        String output = new String();
        while (myReader.hasNextLine()) {
            output = output.concat(myReader.nextLine());
        }
        return output;
    }
}
