package helper;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.recepies.challenge.model.category.Category;

public class CreateCategoryObject {
    public static Category generateCategory() {
        ObjectMapper mapper = new ObjectMapper().configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        String expectedCategory = "{\"id\":\"290141be-6ca4-420c-a7e6-cf6bfcfa0380\",\"name\":\"Soups\",\"classification\":\"category\",\"lastModified\":\"2019-08-19T22:47:14.808Z\",\"created\":\"2019-08-19T22:47:14.808Z\",\"creatorId\":\"1ec1646c-8a12-45f3-a3de-16f145dc74c7\",\"path\":\"/Recipe Categories/Soups\"}";
        Category mockCategory = new Category();
        try {
            mockCategory = mapper.readValue(expectedCategory, Category.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mockCategory;
    }
}
