package com.recepies.challenge.unitTest;

import com.recepies.challenge.model.category.Category;
import com.recepies.challenge.model.content.Content;
import com.recepies.challenge.model.content.ContentRoot;
import com.recepies.challenge.service.MergeService;
import helper.CreateCategoryObject;
import helper.CreateContentObject;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

public class MergeServiceTest {
    @Test
    @Tag("TC_5")
    void getCategorySumTest() {
        System.out.println(Thread.currentThread().getName() + " => getCategorySumTest");
        MergeService mergeService = new MergeService();
        mergeService.getCategorySum(generateCategories(), generateContentRoot());
        Assertions.assertEquals(1, mergeService.mergedContentList.size());
        Assertions.assertTrue(mergeService.mergedContentList.get(0).getDocument().getElements().getCategory().path.contains("/Recipe Categories/Soups"));
    }

    private ContentRoot generateContentRoot() {
        ContentRoot contentRoot = new ContentRoot();
        List<Content> contents = new ArrayList<>();
        contents.add(CreateContentObject.generateContent());
        contentRoot.setDocuments(contents);
        return contentRoot;
    }

    private List<Category> generateCategories() {
        List<Category> categoryList = new ArrayList<>();
        categoryList.add(CreateCategoryObject.generateCategory());
        return categoryList;
    }
}
