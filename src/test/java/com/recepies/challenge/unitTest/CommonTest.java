package com.recepies.challenge.unitTest;

import com.recepies.challenge.service.Common;
import helper.FileReader;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertEquals;

class CommonTest {
    @Test
    @Tag("TC_2")
    void getDataFromEndPoint() throws IOException {
        System.out.println(Thread.currentThread().getName() + " => getDataFromEndPointTest");
        Common common = new Common();
        String expected = new FileReader().readFile("src/test/resources/contentExpected.txt");
        String actualString = common.getDataFromEndPoint("https://content-us-1.content-cms.com/api/06b21b25-591a-4dd3-a189-197363ea3d1f/delivery/v1/search?q=classification:category&rows=100");
        assertEquals(expected, actualString);
    }
}