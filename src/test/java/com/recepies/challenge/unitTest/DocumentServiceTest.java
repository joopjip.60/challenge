package com.recepies.challenge.unitTest;

import com.recepies.challenge.service.DocumentService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

public class DocumentServiceTest {

    @Test
    @Tag("TC_4")
    void getDocumentFromEndpointTest() {
        System.out.println(Thread.currentThread().getName() + " => getDocumentFromEndpointTest");
        DocumentService documentService = new DocumentService();
        documentService.getDocumentFromEndpoint();
        Assertions.assertEquals(83, documentService.contentRoot.numFound);
        Assertions.assertEquals(83, documentService.contentRoot.documents.size());
    }
}
