package com.recepies.challenge.unitTest;

import com.recepies.challenge.model.category.Category;
import com.recepies.challenge.service.CategoryService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

public class CategoryServiceTest {
    Category category = new Category("bf37ba27-1bd6-4df9-89c0-67aa19abf2ad",
            "Cane Sugar Free",
            "category",
            "2019-08-19T22:47:14.233Z",
            "2019-08-19T22:47:14.233Z",
            "1ec1646c-8a12-45f3-a3de-16f145dc74c7",
            "/Nutritional Features/Cane Sugar Free"
    );

    @Test
    @Tag("TC_1")
    void testCategoryService() {
        System.out.println(Thread.currentThread().getName() + " => testCategoryService");
        CategoryService categoryService = new CategoryService();
        categoryService.getCategoryFromEndPoint();
        Assertions.assertTrue(categoryService.categories.contains(this.category));
        Assertions.assertEquals(categoryService.categories.size(), 51);
    }
}
