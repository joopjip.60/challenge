package com.recepies.challenge.unitTest;

import com.recepies.challenge.model.categoryCount.CategoryCount;
import com.recepies.challenge.service.CategoryService;
import com.recepies.challenge.service.CountDocumentService;
import com.recepies.challenge.service.MergeService;
import helper.CreateCategoryObject;
import helper.CreateContentObject;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

@ExtendWith(MockitoExtension.class)
public class CountDocumentTest {

    @Mock
    private MergeService mergeService;

    @Mock
    private CategoryService categoryService;

    @InjectMocks
    private CountDocumentService countDocumentService;

    @Test
    @Tag("TC_3")
    void documentPerCategory() {
        System.out.println(Thread.currentThread().getName() + " => documentPerCategory");
        // create content mock
        mergeService.mergedContentList = new ArrayList<>();
        mergeService.mergedContentList.add(CreateContentObject.generateContent());

        // create mock category
        categoryService.categories = new ArrayList<>();
        categoryService.categories.add(CreateCategoryObject.generateCategory());

        // test Category count
        List<CategoryCount> actual = countDocumentService.documentPerCategory();
        CategoryCount expectedResult = new CategoryCount();
        expectedResult.setCategory_id("290141be-6ca4-420c-a7e6-cf6bfcfa0380");
        expectedResult.setDocuments(1);
        expectedResult.setPath("/Recipe Categories/Soups");

        Assertions.assertEquals(1, actual.size());
        Assertions.assertTrue(actual.contains(expectedResult));

        // check json file is created
        File f = new File("count.json");
        Assertions.assertTrue(f.exists());

    }
}
